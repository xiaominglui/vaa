﻿      var words = [];
      var refreshRate = localStorage.refreshRate || 300; // 5 min default.
      var pollIntervalMin = 1000 * refreshRate;
      var requests = [];
      var wordListName = 'MyWords';
	  //var wordListName = 'MyWords2';
	  var SPEAKER_ICON_URL = chrome.extension.getURL('img/speaker.png');
	  var levelName = ["new", "1st", "2nd", "3rd", "4th", "5th", "6th", "7th", "8th"];
      var bingAppId = 'E31EE8C8CFD0CD0D618059BFC90E17AB4464CE44';
     // var flickrAppKey = '329f9136f1d1d0cdfdd3e476b2824fe2';
      var myport;
      function setIcon(opt_badgeObj) {
      	if (opt_badgeObj) {
      		var badgeOpts = {};
      		if (opt_badgeObj && opt_badgeObj.text != undefined) {
      			badgeOpts['text'] = opt_badgeObj.text;
      		}
      		if (opt_badgeObj && opt_badgeObj.tabId) {
      			badgeOpts['tabId'] = opt_badgeObj.tabId;
      		}
      		chrome.browserAction.setBadgeText(badgeOpts);
      	}
      };

      function clearPendingRequests() {
      	for (var i = 0, req; req = requests[i]; ++i) {
      		window.clearTimeout(req);
      	}
      	requests = [];
      };

      function logout() {
      	words = [];
      	setIcon({
      		'text' : ''
      	});
      	oauth.clearTokens();
      	clearPendingRequests();
      };

      chrome.extension.onMessage.addListener(function (request, sender, sendResponse) {
      
      	if (request.type == 'currentstate') {
      		
      		sendResponse({
      			result : localStorage["board_id"]
      		});
      	}
      });
	  
	  function genericOnClick(info, tab) {
	  	//console.log("item " + info.menuItemId + " was clicked");
	  	//console.log("info: " + JSON.stringify(info));
	  	//console.log("tab: " + JSON.stringify(tab));
	  	console.log(info.selectionText);
	  	myport.postMessage({
	  		message : "menuword",
	  		menuword : "" + info.selectionText
	  	});
	  	//translateEnglishWordByVaa(info.selectionText.toLowerCase());
	  }

	  var contextMenusTitle = "使用VAA查询";
	  var id = chrome.contextMenus.create({
	  		"title" : contextMenusTitle,
	  		"contexts" : ["selection"],
	  		"onclick" : genericOnClick
	  	});
	  
      function addCard(idList, word, result) {
      	//alert("add card");
		//var body = constructTrelloBody(word, result);
      	$.post("https://api.trello.com/1/cards/", {
      		name : word,
      		desc : result,
      		idList : idList,
			pos : "top",
      		key : "d8b28623f3171a9dbc870738cd5f6926",
      		token : localStorage["trello_token"]
      	}, function (card) {
			addDue(card.id);
		});
      }
	  
	  function addSentence(idList, src, result) {
      	//alert("add card");
		//var body = constructTrelloBody(word, result);
      	$.post("https://api.trello.com/1/cards/", {
      		name : src,
      		desc : result,
      		idList : idList,
			labels : "green",
			pos : "top",
      		key : "d8b28623f3171a9dbc870738cd5f6926",
      		token : localStorage["trello_token"]
      	}, function (card) {
			myport.postMessage({
      				message : "collect",
      				txt : "success"
      			});
		});
      }
	  
	  function addDue(card_id) {
	  	var now = new Date();
		now.setMinutes(now.getMinutes() + 5);
		$.ajax({
	  		url : 'https://api.trello.com/1/cards/'+ card_id,
	  		type : 'PUT',
			data : {
      			due : now.toISOString(),
				key : "d8b28623f3171a9dbc870738cd5f6926",
				token : localStorage["trello_token"]
      		},
	  		success : function (result) {
	  			// Do something with the result
	  		}
	  	});
	  }

	  //PUT /1/cards/CARDID?closed=true&idList=newListID&due=now+5
	  function reOpenCard(card_id){
		var now = new Date();
		now.setMinutes(now.getMinutes() + 5);
		
		$.ajax({
	  		url : 'https://api.trello.com/1/cards/'+ card_id,
	  		type : 'PUT',
			data : {
      			closed : false,
				idList :　localStorage["new"],
				due : now.toISOString(),
				key : "d8b28623f3171a9dbc870738cd5f6926",
				token : localStorage["trello_token"]
      		},
	  		success : function (result) {
	  			// Do something with the result
	  		}
	  	});
	  }


      //var myport;
      var skTemp = false;
      var ckTemp = true; ;
      var akTemp = false;
      var qckTemp = true; ;
      var qakTemp = false;
	  var lg = "zh";
      if (localStorage.getItem("sk") == "true") {
      	skTemp = true;
      }
      if (localStorage.getItem("ck") == "false") {
      	ckTemp = false;
      }
      if (localStorage.getItem("ak") == "true") {
      	akTemp = true;
      }
      if (localStorage.getItem("qck") == "false") {
      	qckTemp = false;
      }
      if (localStorage.getItem("qak") == "true") {
      	qakTemp = true;
      }
	  if (localStorage.getItem("lg")) {
      	lg = localStorage.getItem("lg");
      }
      var dictionary = "Dict";
      if (localStorage.getItem("dic") == "Google") {
      	dictionary = "Google";
      }
      var txt = "";
      chrome.extension.onConnect.addListener(onPortConnect);
      function onPortConnect(a) {
      	console.log("backgroud onportconnect" + a.name);
      	if (a.name != "wordterminate") {
      		return false;
      	} else {
      		myport = a;
      		a.onMessage.addListener(onMessageRecieved);
      	}
      }
      function onMessageRecieved(a) {
      	if (a.message == "translate") {
      		console.log("get word" + a.txt);
      		txt = a.txt;
      		goTrasnlate();
      	} else if (a.message == "select")
      		txt = a.txt;
      	else if (a.message == "options") {
      		skTemp = a.Shift;
      		ckTemp = a.Ctrl;
      		akTemp = a.Alt;
			lg = a.Lg;
      		qckTemp = a.QkCtrl;
      		qakTemp = a.QkAlt;
      		//dictionary = a.Dic;
      		localStorage.setItem("sk", a.Shift);
      		localStorage.setItem("ck", a.Ctrl);
      		localStorage.setItem("ak", a.Alt);
      		//localStorage.setItem("dic", a.Dic);
      		localStorage.setItem("lg", a.Lg);
      		
			localStorage.setItem("qkck", a.QkCtrl);
			localStorage.setItem("qkak", a.QkAlt);
      		localStorage.setItem("allNone", a.AllNone);
      	} else if (a.message == "currentsc") {
      		myport.postMessage({
      			message : "currentsc",
      			sk : skTemp,
      			ck : ckTemp,
      			ak : akTemp,
      			qck : qckTemp,
      			qak : qakTemp
      		});
      	} else if (a.message == "close") {
      		chrome.tabs.getSelected(null, function (tab) {
      			chrome.tabs.remove(tab.id);
      			return true;
      		});
      	} else if (a.message == "collect") {
			//alert(a.srcsentence);
			//alert(a.resultsentence);
			addSentence(localStorage["new"], a.srcsentence, a.resultsentence);
		}
      }
      /*
      function name:goTrasnlate
      function use :if the text is an english word,send a xmlHttpRequest to dict and deal with it;else do others
       */
      function goTrasnlate() {
      	if (txt == "")
      		return false;
      	var oneLineTxt = txt.replace(/[\r\n]/g, " ");
      	oneLineTxt = oneLineTxt.replace(/(^\s+)|(\s+$)/g, "");
      	if (oneLineTxt == "")
      		return false;
      	if (/^[a-zA-Z]+$/.test(oneLineTxt)) {
      		if (lg == "zh") {
      			//translateEnglishWordByIciba(oneLineTxt.toLowerCase());
				translateEnglishWordByVaa(oneLineTxt.toLowerCase());
      		} else
      			translateEnglishWordByWordreference(oneLineTxt.toLowerCase(), lg);
      	} else {
      		detectSentence(txt);
      	}
      	txt = "";
      }
	  
	  
      function detectSentence(txt) {
      	$.ajax({
      		url : "http://api.microsofttranslator.com/V2/Ajax.svc/Detect",
      		dataType : "text",
      		data : {
      			appId : bingAppId,
      			text : txt
      		},
      		success : function (response) {
      			var languageFrom = (response == '"en"' ? 'en' : 'zh-CHS');
      			var languageTo = (response == '"en"' ? 'zh-CHS' : 'en');
      			translateSentence(languageFrom, languageTo, txt);

      		}
      	});

      }

      function translateSentence(languageFrom, languageTo, txt) {
      	$.ajax({
      		url : "http://api.microsofttranslator.com/V2/Ajax.svc/Translate",
      		dataType : "text",
      		data : {
      			appId : bingAppId,
      			from : languageFrom,
      			to : languageTo,
      			text : txt
      		},
      		success : function (result) {
      			var buffer = [];
      			buffer.push('<div class="vaa_word">');
      			buffer.push('<div class="vaa_head"><span>' + chrome.i18n.getMessage("showSentenceMeaning") + '</span><span style="float:right; margin-right:10px"><img id="collection" width=25 height=25 /></span></div>');
      			buffer.push('<div class="vaa_content"><div class="vaa_innercontent">');
      			buffer.push('<div class="vaa_meaning"><b>' + chrome.i18n.getMessage("showOriginalText") + '</b><br>' + txt.chunk(60) + '</div><br>');
      			buffer.push('<div class="vaa_example"><b>' + chrome.i18n.getMessage("showTranslationResults") + '</b><br><span id="resultsentence">' + result + '</span></div>');
      			buffer.push('</div></div></div>');
      			myport.postMessage({
      				message : "translate",
      				txt : "" + buffer.join('')
      			});
				
      		}
      	});

      }

	  function translateEnglishWordByVaa(txt) {
      	$.ajax({
      		//url : "http://mili3.herokuapp.com/words/" + txt +".json",
			url : "http://mili.cmlife.com.cn:8090/words/" + txt +".json",
			
      		dataType : "json",
      		success : function (result) {
				var showContent = "";
				if (result) {
					//alert(result.accettation[0].accep);
	
					var def = [];
					var example = [];
					
					if (result.accettation && result.accettation.length > 0) {
						var defbuffer = result.accettation;
						var len = result.accettation.length;
						for (var j = 0; j < len; j++) {
							def.push('<div style="padding-top:6px;"><strong style="padding-right:10px; color:hotpink;">');
							def.push(defbuffer[j].pos);
							def.push('.&nbsp</strong>');
							def.push(defbuffer[j].accep);
							def.push('</div>');
						}
					}
					//<span class="pron">' + showpron + '</span>&nbsp&nbsp<span class="audio" data-src="' + mp3 + '"><img id="speaker"/></span>
					//alert(result.pron.length);
					
					var pron = [];
					var pron_en;
					var pron_us;
					//alert(result.pron[0].link);
					if(result.pron && result.pron.length > 0){
						var pronbuffer = result.pron;
						var plen = result.pron.length;
						for (var i = 0; i < plen; i++) {
							if (pronbuffer[i].ps) {
								pron.push('<span class="pron">');
								pron.push(pronbuffer[i].type == "en" ? "英 " : "美 ");
								pron.push('<strong class="pronce">[');
								pron.push(pronbuffer[i].ps);
								pron.push(']</strong></span>&nbsp&nbsp<span class="audio" data-src="');
								pron.push(pronbuffer[i].link);
								pron.push('"><img class="pronimg" id="speaker' + i + '"/></span>');
								if(pronbuffer[i].type == "en"){
									pron_en = pronbuffer[i].link;
								}else {
									pron_us = pronbuffer[i].link;
								}
							}
						}
					}
					
					showContent = consturctShow(txt, pron.join(''), def.join(''), example.join(''), result.pic);
					//console.log(showContent);
					var des= JSON.stringify(result);
					
      				/*myport.postMessage({
      					message : "translate",
      					txt : "" + showContent,
      					mp3url_en : pron_en,
						mp3url_us : pron_us
      				});*/
					isSaveWord(txt, des, 0, "noUpdate");
					
				} else {
					var buffer = [];
      				buffer.push('<div class="vaa_word">');
      				buffer.push('<div class="vaa_head"></div>');
      				buffer.push('<div class="vaa_content"><div class="vaa_innercontent">');
      				buffer.push('<div class="vaa_meaning" style="text-align:left; font-weight:bold;">word is not found</div><br>');
      				buffer.push('<div class="vaa_example"></div>');
      				buffer.push('</div></div></div>');
					showContent = buffer.join('');
      				
				}
				myport.postMessage({
      					message : "translate",
      					txt : "" + showContent,
      					mp3url_en : pron_en,
						mp3url_us : pron_us
      				});
      		},
			error : function (xhr, type, exception) { //获取ajax的错误信息
				//alert(xhr.responseText, "Failed");
				var buffer = [];
				buffer.push('<div class="vaa_word">');
				buffer.push('<div class="vaa_head"></div>');
				buffer.push('<div class="vaa_content"><div class="vaa_innercontent">');
				buffer.push('<div class="vaa_meaning" style="text-align:left; font-weight:bold;">' + xhr.responseText + '</div><br>');
				buffer.push('<div class="vaa_example"></div>');
				buffer.push('</div></div></div>');
				showContent = buffer.join('');
				myport.postMessage({
					message : "translate",
					txt : "" + showContent,
					mp3url_en : pron_en,
					mp3url_us : pron_us
				});
			}
      	});
      }
	  
	  
	  function consturctShow(txt, showpron, def, example, pic) {
	  	var buffer = [];
	  	buffer.push('<div class="vaa_word">');
	  	buffer.push('<div class="vaa_head"><div class="wordname">' + txt + '</div></div>');
	  	buffer.push('<div class="vaa_content"><div class="vaa_innercontent">');
	  	pic ? buffer.push('<img src="'+ pic +'" style="float:right;width:30%;margin:0px 10px;"/>'):buffer.push();
		buffer.push('<div class="pron">' + showpron + '</div><br>');
		//def.length == 0?"":buffer.push('<div class="vaa_meaning"><b>' + chrome.i18n.getMessage("showWordMeaning") + '</b><br>' + def + '</div><br>');
	  	//example.length == 0? "":buffer.push('<div class="vaa_example"><b>' + chrome.i18n.getMessage("showWordExample") + '</b><br>' + example + '</div>');
	  	def.length == 0?buffer.push('<div class="vaa_meaning">暂无解释</div><br>'):buffer.push('<div class="vaa_meaning">' + def + '</div><br>');
	  	example.length == 0? buffer.push('<div class="vaa_meaning"></div><br>'):buffer.push('<div class="vaa_example"><b>' + chrome.i18n.getMessage("showWordExample") + '</b><br>' + example + '</div>');
		buffer.push('</div></div></div>');
		return buffer.join('');
	  }
	  
	  
      String.prototype.chunk = function (n) {
      	if (n < 0 || this.length < n) {
      		return this;
      	} else if (n > 0) {
      		return this.substring(0, n) + '...';
      	}
      }
	  
	  function isSaveWord(word, result, level, type) {
		var levelnum = levelName[level];
	  	if (localStorage['temp_wordlist'].length > 0) {
	  		var tempWordList = JSON.parse(localStorage['temp_wordlist']);
	  		//alert(tempWordList);
	  		for (var i = 0; i < tempWordList.length; i++) {
	  			if (tempWordList[i] == word) {
	  				return true;
	  			}
	  		}
	  		tempWordList[tempWordList.length] = word;
	  		//alert(tempWordList);
	  		localStorage['temp_wordlist'] = JSON.stringify(tempWordList);
	  		//addWord(word, pron, definit, exampleString, mp3);
	  		
			
	  		addCard(localStorage[levelnum], word, result);
			
			return false;
	  	} else {
	  		var newTempWordList = [];
	  		newTempWordList.push(word);
	  		localStorage['temp_wordlist'] = JSON.stringify(newTempWordList);
	  		addCard(localStorage[levelnum], word, result);
	  	}
	  }
     
	 setInterval(function () {
	 	console.log("test");
		if (localStorage["service"] == "Trello") {
	 		//alert("setinterval");
	 		$.get("https://api.trello.com/1/boards/" + localStorage["board_id"] + "/cards", {
	 			filter : "open",
	 			key : "d8b28623f3171a9dbc870738cd5f6926",
	 			token : localStorage["trello_token"]
	 		}, function (cards) {
				localStorage['myWords'] = JSON.stringify(cards);
	 			if (cards.length == 0) {
	 				setIcon({
	 					'text' : ''
	 				});
	 			} else {
	 				var word_num = 0;
	 				$.each(cards, function (ix, card) {
	 					if (card.desc !== "null" && card.due) {
	 						var due = new Date(card.due);
	 						var now = new Date();
	 						if (now >= due) {
	 							word_num++;
	 						}

	 					}
	 				});
	 				console.log("wordnum" + word_num);
	 				setIcon({
	 					'text' : word_num == 0 ? "" : word_num.toString()
	 				});
	 			}
	 		});
	 	}
      }, pollIntervalMin);
	  
	  