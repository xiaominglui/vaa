﻿var bgPage = chrome.extension.getBackgroundPage();
var VAAOperation = {
	list_mode : chrome.i18n.getMessage("list_mode"),
	pic_mode : chrome.i18n.getMessage("pic_mode"),
	remember : chrome.i18n.getMessage("remembered"),
	showmeaning : chrome.i18n.getMessage("showmeaning"),
	deleteword : chrome.i18n.getMessage("deleteword"),
	refresh : chrome.i18n.getMessage("refresh"),
	rss : chrome.i18n.getMessage("RSS"),
	option : chrome.i18n.getMessage("option"),
	nowordremember : chrome.i18n.getMessage("nowordremember"),
	greatjob : chrome.i18n.getMessage("greatjob"),
    //creatvocabulary_displayMsg : chrome.i18n.getMessage("creatvocabulary_displayMsg"),
	no_newwords : chrome.i18n.getMessage("no_newwords"),

	//bgPage : chrome.extension.getBackgroundPage(),
	//pollIntervalMax : 1000 * 60 * 60, // 1 hour
	cards_num : 0,
	BOARD_NAME : bgPage.wordListName,
	modelchoice : "listmodel",
	slider : null,
	
	
	listsName : ["new", "1st", "2nd", "3rd", "4th", "5th", "6th", "7th", "8th"],
	listsLevel : {
		"new" : 0,
		"1st" : 1,
		"2nd" : 2,
		"3rd" : 3,
		"4th" : 4,
		"5th" : 5,
		"6th" : 6,
		"7th" : 7,
		"8th" : 8
	},
	
	//trello Authorize operation
	
	trelloAuthorizeSuccess : function () {
		//if (bgPage.updateFlag) {
			//util.displayMsg("update.....");
		//} else {
			//alert("trello");
			localStorage['temp_wordlist'] = "";
			util.loadingDataPic(); //util.displayMsg(wait_moment);
			$cardlist = $("<ul>")
				.addClass("bxslider")
				.addClass("box")
				.appendTo("#output");
			VAAOperation.slider = $('.box').bxSlider({
					adaptiveHeight : true,
					slideWidth : 355,
					minSlides : 1,
					maxSlides : 1,
					slideMargin : 10,
					pager : false, 
					controls : false,
					infiniteLoop : false,
					hideControlOnEnd : true
					
				});
			if (localStorage["board_id"] && localStorage["1st"] && localStorage["2nd"] && localStorage["3rd"] && localStorage["4th"] && localStorage["5th"] && localStorage["6th"] && localStorage["7th"] && localStorage["8th"] && localStorage["new"]) {
				
				if (VAAOperation.modelchoice == 'listmodel') {
					if(localStorage['myWords']){
						util.loadingDataPic();
						var localCards = JSON.parse(localStorage['myWords']);
						VAAOperation.showWordsCards(localCards);
					}else{
						VAAOperation.constructListModelContent();
					}
				} else {
					VAAOperation.constructPicModelContent();
				}

			} else {
				VAAOperation.isBoardExit();
			}
		//}
	},
	
	trelloAuthorize : function () {
		var trellourl = chrome.extension.getURL('connect.html');
		chrome.tabs.create({
			'url' : trellourl
		});
		Trello.authorize({
			interactive : false,
			success : VAAOperation.trelloAuthorizeSuccess
		});
	},
	

	showMessage : function(){
		if (VAAOperation.slider) {
			VAAOperation.slider.destroySlider();
		};
		//VAAOperation.slider.destroySlider();
		//console.log("message" + VAAOperation.greatjob);
		$cardlist.html("");
		var $noword = $("<div>")
			.addClass("nowordcontainer")
			.html('<div class="nowordtoremember"></div><div class="great">' + VAAOperation.greatjob + '</div>')
			.appendTo($cardlist);

	},


	//trello operation
	changeBoardClosedStatus : function (board_id, status, type) {
		Trello.put("boards/" + board_id + "/closed", {
			value : status
		}, function (board) {
			if (type == "addBoard") {
				for (var i = 0; i < VAAOperation.listsName.length; i++) {
					VAAOperation.addList(board.id, VAAOperation.listsName[i], VAAOperation.listsLevel[VAAOperation.listsName[i]]);
				}
				$('#butter1').fadeOut(0);
				
				VAAOperation.showMessage();




				//util.displayMsg(VAAOperation.no_newwords);
			} else if (type == "changeStatus") {
				VAAOperation.getListCard(board.id);
			}
		}, function (error) {
			console.log("error:" + error);
			if (type == "changeStatus") {
				VAAOperation.getListCard(board_id);
			}

		});
	},
	
	changeListClosedStatus : function (list_id, status) {
		Trello.put("lists/" + list_id + "/closed", {
			value : status
		}, function (list) {

		});
	},
	
	addBoard : function () {
		Trello.post("boards", {
			name : VAAOperation.BOARD_NAME,
			desc : "TrelloDB"
		}, function (boards) {
			localStorage["board_id"] = boards.id;
			VAAOperation.changeBoardClosedStatus(boards.id, true, "addBoard");
		});
	},
	
	addList : function (idBoard, name, levelnum) {
		Trello.post("lists", {
			name : name,
			idBoard : idBoard
		}, function (list) {
			//console.log(name + "added");
			localStorage[name] = list.id;
			localStorage[list.id] = levelnum;
			//changeListClosedStatus(list.id, true);
		});
	},
	
	getListCard : function (board_id) {
		Trello.get("boards/" + board_id + "/lists", {
			filter : "all"
		}, function (lists) {
			var listName = [];
			var listsNameString = VAAOperation.listsName.toString();
			var needChangeStatuesList = [];
			for (var i = 0; i < lists.length; i++) {
				listName.push(lists[i].name);
				if (listsNameString.indexOf(lists[i].name) !== -1) {
					if (lists[i].closed) {
						needChangeStatuesList.push(lists[i].id);
					}
					localStorage[lists[i].name] = lists[i].id;
					localStorage[lists[i].id] = VAAOperation.listsLevel[lists[i].name];
				}
			}
			var needCreateListName = util.array_diff(VAAOperation.listsName, listName);
			if (needChangeStatuesList.length > 0) {
				for (var i = 0; i < needChangeStatuesList.length; i++) {
					VAAOperation.changeListClosedStatus(needChangeStatuesList[i], false);
				}
			}
			if (needCreateListName.length <= VAAOperation.listsName.length) {
				for (var i = 0; i < needCreateListName.length; i++) {
					VAAOperation.addList(board_id, needCreateListName[i]);

				}
			}
			
			if (VAAOperation.modelchoice == 'listmodel') {
				VAAOperation.constructListModelContent();
			} else {
				VAAOperation.constructPicModelContent();
			}
		});
	},
	
	closedCard : function (card_id, status) {
		Trello.put("cards/" + card_id + "/closed", {
			value : status
		}, function (data) {});
	},
	
	changeCardList : function (card_id, list_id, time) {
		Trello.put("cards/" + card_id, {
			idList : list_id,
			pos : "top",
			due : time
		}, function (data) {
			//console.log("finishe");
		});
	},
	
	isBoardExit : function () {
		console.log("isborderexit");
		Trello.get("members/me/boards", function (boards) {
			console.log("isborderexit");
			for (var i = 0; i < boards.length; i++) {
				if (boards[i].name == VAAOperation.BOARD_NAME && boards[i].desc == "TrelloDB") {
					localStorage["board_id"] = boards[i].id;
					boards[i].closed ? VAAOperation.getListCard(boards[i].id) : VAAOperation.changeBoardClosedStatus(boards[i].id, true, "changeStatus");
					return;
				}
			}
			VAAOperation.addBoard();

		});
	},
	
	constructFamiliarityImage : function (name, level) {
		var degree = parseInt(level);
		var img = [];
		for (var j = 0; j < 9 - degree; j++) {
			//console.log(name + ": unlevel");
			img.push('<img src="img/icons/default/16/unlevel.png" alt="Rememered" width="15px" height="15px" class="level" style="cursor: pointer;">');
		}
		for (var i = 0; i < degree; i++) {
			//console.log(name + ": level");
			img.push('<img src="img/icons/default/16/level.png" alt="Rememered" width="15px" height="15px" class="level" style="cursor: pointer;">');
		}
		return img.join('');
	},
	
	/*constructPopupEWordContent : function (card, modle) {
		var content = [];
		var desc = JSON.parse(card);

		if (modle == "listview") {
			if (desc.pron.length > 0) {
				content.push("<div style='color:#4E7DC2; width:100%; border-top:1px solid #ddd;padding-top:5px;font-weight:normal;font-size:15px;'>");
				for (var i = 0; i < desc.pron.length; i++) {
					if (desc.pron[i].ps) {
						content.push("<span>")
						content.push(desc.pron[i].type == "en" ? "英 " : "美 ");
						content.push('[' + desc.pron[i].ps + ']');
						content.push('&nbsp&nbsp<span class="audio" data-src="');
						content.push(desc.pron[i].link);
						content.push('"><img id="speaker" src="img/speaker.png" class="' + desc.pron[i].link + '"/></span></span>&nbsp&nbsp');

					}
				}
				content.push("</div>");
			}
			if (desc.accettation.length > 0) {
				content.push('<div>');
				for (var j = 0; j < desc.accettation.length; j++) {
					content.push('<div style="padding-top:6px; line-height:1.5;"><strong style="padding-right:10px;">');
					content.push(desc.accettation[j].pos + ".");
					content.push('&nbsp</strong>');
					content.push(desc.accettation[j].accep + '</div>');
				}
				content.push('</div>');
			}

		} else {
			content.push("<div style='color:#4E7DC2; width:90%'><MARQUEE direction=left behavior=alternate scrollamount=1 scrolldelay=100>" + mean[1] + "</MARQUEE></div><br>")
			content.push("<div><b>" + mean[2] + "</b><br>");
			content.push(mean[3].chunk(80) + "</div>");
		}
		return content.join('');
	},*/
	
	constructPopupEWordHead : function (card, wordname) {
		var content = [];
		var desc = JSON.parse(card);
		//content.push("<div class='head'>");
		content.push("<span>" + wordname + "</span>");
		//content.push("<span class='see'><img src='img/icons/default/16/seemean.png'/></span>");
		
		//content.push("<span class='remmeber'><img src='img/icons/default/16/rem.png'/></span>");
		//content.push("</div>");
		return content.join('');
	},
	
	constructPopupEWordContent : function (card, wordname) {
		var content = [];
		var desc = JSON.parse(card);
		//content.push("<div style='font-size:xx-large;'><span>" + wordname + "<span><span style='float:right;margin-right:15px;'><img class='delete' style='width: 20px; height: 20px;' src='img/icons/default/16/delete.png'></span></div>");
		if (desc.pron && desc.pron.length > 0) {
			content.push("<div class='pron'>");
			for (var i = 0; i < desc.pron.length; i++) {
					if (desc.pron[i].ps) {
						content.push("<span>")
						content.push(desc.pron[i].type == "en" ? "英 " : "美 ");
						content.push('[' + desc.pron[i].ps + ']');
						content.push('<span class="audio" data-src="');
						content.push(desc.pron[i].link);
						content.push('"><img id="speaker" src="img/speaker.png" class="' + desc.pron[i].link + '"/></span></span>');
					}
				}
			content.push("</div>");
		}
		content.push("<br>");
		if (desc.accettation && desc.accettation.length > 0) {
			content.push("<div>");
			for (var j = 0; j < desc.accettation.length; j++) {
				content.push("<div class='wordmean'>");
				content.push("<strong>" + desc.accettation[j].pos + "</strong>");
				content.push("<font>" + desc.accettation[j].accep + "</font>");
				content.push("</div>");
			}
			content.push("</div>");
		}
		content.push("<br>");
		if (desc.examples && desc.examples.length > 0) {
			content.push("<ul class='vaa_example'>");
			for (var k = 0; k < desc.examples.length; k++) {
				content.push("<li>" + desc.examples[k].sentence + "<br>");
				content.push(desc.examples[k].explain + "</li>");
			}
			content.push("</ul><br>");
		}
		
		return content.join('');
	},
	


	
	showWordsCards : function (cards) {
		$cardlist.html(" ");
		var word_num;
		var tempWordList = [];
		var tempAllWordList = [];
		if (cards.length == 0) {
			$('#butter1').fadeOut(0);
			//util.displayMsg(VAAOperation.no_newwords);
			VAAOperation.showMessage()

			bgPage.setIcon({
				'text' : cards.length == 0 ? "" : cards.length.toString()
			});

		} else {
			word_num = 0;
			$.each(cards, function (ix, card) {
				if (card && card.desc !== "null" && card.due) {
					tempWordList.push(card.name);
					tempAllWordList.push(card);
					var cardDesc = card.desc.replace(/\\n/g, "");
					var cardcontent = JSON.parse(cardDesc);
					var due = new Date(card.due);
					var now = new Date();
					//console.log(card.name);
					//console.log(due + " :" + now);
					if (now >= due) {
						//console.log(card.name);
						word_num++;
						var $wordcontainer = $("<li>")
							.appendTo($cardlist);

						var pic = cardcontent.pic ? cardcontent.pic : "img/icons/default/16/bg.png";
						var $wordtitle = $("<div>")
							.addClass("word")
							.css("background", "url(" + pic +")")
							.css("background-position", "center")
							.css("background-repeat", "no-repeat")
							.appendTo($wordcontainer);

						var $wordtitle2 = $("<div>")
							.addClass("containt")
							//.html(VAAOperation.constructPopupEWordHead(cardDesc, card.name) + VAAOperation.constructPopupEWordContent(cardDesc, card.name))
							.appendTo($wordtitle);

						var $wordhead = $("<div>")
							.addClass("head")
							.html(VAAOperation.constructPopupEWordHead(cardDesc, card.name))
							.appendTo($wordtitle2);

						var $rem = $("<span>")
							.addClass("remmeber")
							.attr("id", card.idList + "_" + card.id)
							.html("<img src='img/icons/default/16/rem.png'/>")
							.appendTo($wordhead);

						var $wordhead = $("<div>")
							.addClass("mean")
							.html(VAAOperation.constructPopupEWordContent(cardDesc, card.name))
							.appendTo($wordtitle2);
					}
				}
			});
			$('#butter1').fadeOut(0);
			VAAOperation.slider.reloadSlider({
				adaptiveHeight : true,
				slideWidth : 355,
				minSlides : 1,
				maxSlides : 1,
				slideMargin : 10,
				pager : true,
				pagerType : 'short',
				controls : true,
				infiniteLoop : false,
				hideControlOnEnd : true
			});
			//console.log("slide" + $('.box').html());
			$(".word").click(function (e) {
				//console.log("word click");
				$(this).children("div.containt").addClass("show");
				$(this).find("span.remmeber").find("img").attr("src","img/icons/default/16/seemean.png")
					
				$(this).find("span.remmeber").removeClass("remmeber").unbind("click").addClass("see").bind("click", function (e) {
					console.log(VAAOperation.slider.getCurrentSlide());
					console.log(VAAOperation.slider.getSlideCount());
					word_num = word_num - 1;
					bgPage.setIcon({
						'text' : word_num == 0 ? "" : word_num.toString()
					});
					if (VAAOperation.slider.getCurrentSlide() + 1 == VAAOperation.slider.getSlideCount()) {
						
						
						VAAOperation.showMessage();

						//VAAOperation.slider.destroySlider();
						//$cardlist.html("");
						//var $noword = $("<div>")
						//	.addClass("nowordcontainer")
						//	.html('<div class="nowordtoremember">' + VAAOperation.nowordremember + '</div><div class="great">' + VAAOperation.greatjob + '</div>')
						// 	.appendTo($cardlist);
					} else {
						VAAOperation.slider.goToNextSlide();
					}
					var cardinfo = $(this).attr("id").split("_");
					//console.log($(this).attr("id"));
					var localCards = JSON.parse(localStorage['myWords']);

					for (var i = 0; i < localCards.length; i++) {
						if (localCards[i].id == cardinfo[1]) {
							console.log(localCards[i]);
							localCards.splice(i, 1);
							break;
						}
					}

					localStorage['myWords'] = JSON.stringify(localCards);
					var time = VAAOperation.setTime(0);
					VAAOperation.changeCardList(cardinfo[1], cardinfo[0], time);
					
				});
			});

			$(".remmeber").unbind("click").bind("click", function (e) {
				console.log(VAAOperation.slider.getCurrentSlide());
				console.log(VAAOperation.slider.getSlideCount());
				event.stopPropagation();
				var current = VAAOperation.slider.getCurrentSlide();
				var parent = $(this).parent().parent().parent().parent();
				var cardinfo = $(this).attr("id").split("_");
				VAAOperation.remembereWord(cardinfo[0], cardinfo[1]);
				var localCards = JSON.parse(localStorage['myWords']);
				
				for(var i = 0 ; i < localCards.length; i++){
					if(localCards[i].id == cardinfo[1]){
						console.log(localCards[i]);
						localCards.splice(i ,1);
						break;
					}
				}
				
				word_num = word_num - 1;
				bgPage.setIcon({
					'text' : word_num == 0 ? "" : word_num.toString()
				});
				
				localStorage['myWords'] = JSON.stringify(localCards);
				if (VAAOperation.slider.getCurrentSlide() + 1 == VAAOperation.slider.getSlideCount()) {
					
					VAAOperation.showMessage();
					//alert("no card");
					// VAAOperation.slider.destroySlider();
					// $cardlist.html("");
					// var $noword = $("<div>")
					// 	.addClass("nowordcontainer")
					// 	.html('<div class="nowordtoremember">' + VAAOperation.nowordremember + '</div><div class="great">' + VAAOperation.greatjob + '</div>')
					// 	.appendTo($cardlist);
				} else {
					parent.animate({
						top : '255px',
						opacity : '0',
					}, 'slow', function () {

						parent.remove();
						VAAOperation.slider.goToNextSlide();

						VAAOperation.slider.reloadSlider({
							adaptiveHeight : true,
							slideWidth : 355,
							minSlides : 1,
							maxSlides : 1,
							slideMargin : 10,
							pager : true,
							pagerType : 'short',
							controls : true,
							infiniteLoop : false,
							hideControlOnEnd : true,
							startSlide : current
						});
					});

				}

				
				
			});

			localStorage['temp_wordlist'] = JSON.stringify(tempWordList);
			if (word_num == 0) {
				
				VAAOperation.showMessage();
				// VAAOperation.slider.destroySlider();
				// $cardlist.html("");
				// var $noword = $("<div>")
				// 	.addClass("nowordcontainer")
				// 	.html('<div class="nowordtoremember">' + VAAOperation.nowordremember + '</div><div class="great">' + VAAOperation.greatjob + '</div>')
				// 	.appendTo($cardlist);
			} else {
				bgPage.setIcon({
					'text' : word_num.toString()
				});
			}
		}
	},
	
	constructListModelContent : function () {
		util.loadingDataPic();
		Trello.get("boards/" + localStorage["board_id"] + "/cards", {
			filter : "open"
		}, function (cards) {
		    localStorage['myWords'] = JSON.stringify(cards);
			VAAOperation.showWordsCards(cards);
		});
	},
	
	timeArray : [5, 30, 720, 1440, 2880, 5760, 10082, 21600, -1],
	setTime : function (level) {
		var date = new Date();
		date.setMinutes(date.getMinutes() + VAAOperation.timeArray[level]);
		return date.toISOString();
	},
	
	remembereWord : function (list_id, card_id) {
		if (parseInt(localStorage[list_id]) == 8) {
			VAAOperation.closedCard(card_id, true);
		} else {
			for (var key in localStorage) {
				//console.log("key: " + key + " value:" + localStorage[key] + " type: " + typeof(localStorage[key]));
				if (parseInt(localStorage[key]) == parseInt(localStorage[list_id]) + 1) {
					//console.log()
					var time = VAAOperation.setTime(parseInt(localStorage[list_id]) + 1);
					VAAOperation.changeCardList(card_id, key, time);
					return;
				}
			}

		}
	},

	openOption : function () {
		chrome.tabs.create({
			url : "options.html"
		});
	}
}

String.prototype.chunk = function (n) {
	if (n < 0 || this.length < n) {
		return this;
	} else if (n > 0) {
		return this.substring(0, n) + '...';
	}
}



